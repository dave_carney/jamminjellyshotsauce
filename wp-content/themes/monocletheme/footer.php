<?php
/* This file outputs the Footer */
?>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-9">

<?php do_action('before_footer_nav'); ?>

				<nav class="footer-nav">
					<?php if(has_nav_menu('footer-menu')) {
			    		wp_nav_menu(array('theme_location' => 'footer-menu'));
					} ?>
				</nav>
	
<?php do_action('after_footer_nav'); ?>

				<div class="social">
					<ul>
						<?php social_output(); ?>
					</ul>
				</div>
			</div> <!-- .col-sm-9 -->

			<div class="col-sm-3">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/certified_cajun.png" class="img-responsive" alt="Certified Cajun certificate">
			</div>
			
			<div class="credits">
				<small>Copyright <?php echo date('Y'); ?> | 
					<a href="<?php echo wp_login_url( $redirect ); ?>">Login</a><span class="hidden-xs"> |</span>
				</small>
				<?php monocle_credits(); ?>
			</div>
		</div> <!-- .row -->

	</div> <!-- .container -->
</footer>
<?php do_action('after_footer'); ?>
<?php wp_footer(); ?>
</body>
</html>