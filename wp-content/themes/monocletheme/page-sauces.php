<?php
/*
Template name: Sauces page
*/
?>

<?php get_header(); ?>
<script>
jQuery(document).ready(function($) {
        var sauceNavStart = $('#sauce-nav-wrapper').offset().top - 80;

        function stickySauceNav() {
            var yPos2 = $(window).scrollTop();
            if (yPos2 > sauceNavStart) {
                $('.sauce-nav').addClass('scroll');
            } else {
                $('.sauce-nav').removeClass('scroll');
            }
        }
        stickySauceNav();

        $(window).scroll(function() {
            stickySauceNav();
        });
});
</script>
<?php do_action('before_main_content'); ?>

    <article class="main-content monocle-all-products sauces-page"> <!-- The main content section -->
        <div class="container">

            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>

        <?php do_action('before_title'); ?>
                    <?php if (get_field('upper_headline')) : ?>
                        <h2 class="title"><?php the_field('upper_headline'); ?></h2>
                    <?php else : ?>
                        <h2 class="title"><?php the_title(); ?></h2>
                    <?php endif; ?>
        <?php do_action('after_title'); ?>

                        <?php if (has_post_thumbnail()) {
                            the_post_thumbnail('bootstrap_fullwidth', array('class' => 'img-responsive')); } ?>

                        <?php if (get_field('lower_headline')) : ?>
                            <h3 class="lower-headline"><?php the_field('lower_headline'); ?></h3>
                        <?php endif; ?>

                        <?php the_content(); ?>
                        <hr>
                
                <?php endwhile; ?>
                <?php endif; ?>

            <div class="row">
                <div <?php post_class('col-sm-10 col-sm-push-2'); ?>>

                <?php
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => -1,
                        'paged' => $paged,
                        'orderby' => 'menu_order',
                        'order' => 'ASC'
                        );
                    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                    $monocle_products_loop = new WP_Query( $args );
                    if ( $monocle_products_loop->have_posts() ) : ?>

                    <?php $monocle_product_titles = array(); ?>

                    <?php woocommerce_product_loop_start(); ?>

                        <?php woocommerce_product_subcategories(); ?>

                            <?php while ( $monocle_products_loop->have_posts() ) : $monocle_products_loop->the_post(); ?>

                                <?php $monocle_product_titles[] = get_field('menu_title'); ?>

                                <?php wc_get_template_part( 'content', 'product-alt' ); ?>

                            <?php endwhile; ?>

                <?php wp_reset_postdata(); ?>
                <?php endif; ?>

                    <?php woocommerce_product_loop_end(); ?>

                </div> <!-- .col-sm-10 -->

                <div id="sauce-nav-wrapper" class="col-sm-2 col-sm-pull-10 hidden-xs">
                    <nav class="sauce-nav">
                        <ul>
                            <?php foreach($monocle_product_titles as $monocle_product_title) {
                                $anchor = str_replace(' ', '-', $monocle_product_title);
                                $anchor = strtolower($anchor);
                                echo '<li><a href="#' . $anchor . '">' . $monocle_product_title . '</a></li>';
                            } ?>
                        </ul>
                    </nav>
                </div>

            </div> <!-- .row -->

            <div class="social-cta">
                <h6>Follow Us:</h6>
                <div class="social">
                    <ul>
                        <?php social_output(); ?>
                    </ul>
                </div>
            </div>

        </div> <!-- .container -->
    </article> <!-- End of the main content section -->

<?php do_action('after_main_content'); ?>
<?php get_footer(); ?>