<?php
/* The template for displaying the latest blog posts */
?>

<?php get_header(); ?>
<?php do_action('before_main_content'); ?>

    <section> <!-- The main content section -->
        <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
        
        <div <?php post_class(); ?>>

<?php do_action('before_title'); ?>
                <h2 class="title"><?php the_title(); ?></h2>
<?php do_action('after_title'); ?>
                <?php the_content(); ?>
        </div>
        
        <?php endwhile; ?>
        <?php endif; ?>
    </section> <!-- End of the main content section -->

<?php do_action('after_main_content'); ?>
<?php get_footer(); ?>