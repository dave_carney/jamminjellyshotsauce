<?php
/*
Template name: Contact page
*/
?>

<?php get_header(); ?>
<?php do_action('before_main_content'); ?>

    <article class="main-content monocle-contact"> <!-- The main content section -->
        <div class="container">

            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            
            <div <?php post_class(); ?>>

    <?php do_action('before_title'); ?>
                <?php if (get_field('upper_headline')) : ?>
                    <h2 class="title"><?php the_field('upper_headline'); ?></h2>
                <?php else : ?>
                    <h2 class="title"><?php the_title(); ?></h2>
                <?php endif; ?>
    <?php do_action('after_title'); ?>

                    <?php if (has_post_thumbnail()) {
                        the_post_thumbnail('bootstrap_fullwidth', array('class' => 'img-responsive')); } ?>

                    <?php if (get_field('lower_headline')) : ?>
                        <h3 class="lower-headline"><?php the_field('lower_headline'); ?></h3>
                    <?php endif; ?>

                    <?php the_content(); ?>

                    <?php if (get_field('map')) {
                        the_field('map'); } ?>
            </div>
            
            <?php endwhile; ?>
            <?php endif; ?>

            <div class="social-cta">
                <h6>Follow Us:</h6>
                <div class="social">
                    <ul>
                        <?php social_output(); ?>
                    </ul>
                </div>
            </div>

        </div> <!-- .container -->
    </article> <!-- End of the main content section -->

<?php do_action('after_main_content'); ?>
<?php get_footer(); ?>