// Javascript functions for Monocle theme

// adds jQuery wrapper so that $ can be used
jQuery(document).ready(function($) {

	// Shopping cart icon
	$('.cart-icon a:not(.sub-menu a)').prepend('<i class="fa fa-shopping-cart"></i> ');

	// Adds img-responsive to select images
	$('img.wp-post-image').addClass('img-responsive');

	// Bootstrap carousel
	$('.carousel-inner div:first-of-type').addClass("active");

	// Adds img-responsive to other images.
	$('.monocle-product-summary img').addClass('img-responsive');
	$('.intro .col-sm-push-6 img').addClass('img-responsive');

	// sticky navbar
		var contentStart = $('#static-nav').offset().top - 16;

		function stickyNav() {
			var yPos = $(window).scrollTop();
			if (yPos > contentStart) {
				$('#sticky-nav').show();
			} else {
				$('#sticky-nav').hide();
			}
		}
		stickyNav();

		$(window).scroll(function() {
			stickyNav();
		});

	// Dropdowns for touchscreens
	$.fn.monocleToggle = function() {
		if (!('ontouchstart' in window) &&
			!navigator.maxTouchPoints &&
			!navigator.userAgent.toLowerCase().match( /windows phone os 7/i )) {
			return false;
		}
		this.each(function() {
			var menu_open = false;

			$(this).on('click', function(e) {
					if (menu_open === false) {
						e.preventDefault();
						$(this).closest('ul').show();
						menu_open = true;
					}
			});
			$(document).on('click touchstart pointerdown', function(e) {
				if (!$(e.target).closest('nav li:has(ul)').length &&
					!$(e.target).is('nav li:has(ul)')) {
						if($('nav li:has(ul)').is(':visible')) {
							$(this).closest('ul').hide();
							menu_open = false;
						}
				}
			});

		});
		return this;
	};

	$('nav li:has(ul)').monocleToggle();

});