<?php
/* The template for the Homepage */
?>

<?php get_header(); ?>
<?php do_action('before_main_content'); ?>

    <section class="main-content front-page"> <!-- The main content section -->
        <div class="container intro">
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            
            <div <?php post_class('row'); ?>>

                <div class="col-sm-6 col-sm-push-6">
                    <h2 class="title text-center">Jammin Jelly's<br>Hot Sauce</h2>
                    <?php the_content(); ?>
                    <a href="order-hot-sauce-from-jammin-jellys" class="btn orange-btn">Order Now!</a>
                </div>

                <div class="col-sm-6 col-sm-pull-6 slider">
                    <div>
                        
                            <?php $images = get_field('homepage_slider');
                                if( $images ): ?>

                                    <div id="homepage-slider" class="carousel slide" data-ride="carousel" data-interval="7000">

                                        <div class="carousel-inner" role="listbox">

                                                <?php foreach( $images as $image ): ?>
                                                    <div class="item">
                                                        <img src="<?php echo $image['sizes']['homepage_slider']; ?>" alt="<?php echo $image['alt']; ?>">
                                                    </div>
                                                <?php endforeach; ?>

                                        </div> <!-- .carousel-inner -->

                                    </div> <!-- #homepage-slider -->

                            <?php endif; ?>

                    </div>
                </div>

            </div> <!-- .row -->
            
            <?php endwhile; ?>
            <?php endif; ?>
        </div> <!-- .container .intro -->

    <?php if (get_field('about_homepage_background', 33)) :
        $about_bg = get_field('about_homepage_background', 33); ?>
        <article class="about" style="background: url('<?php echo $about_bg; ?>') center center no-repeat; background-size: cover;">
        <div class="overlay"></div>
    <?php else : ?>
        <article class="about">
    <?php endif; ?>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">

                        <h3>Who We Are</h3>
                        <img class="hidden-xs" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/certified_cajun.png" class="img-responsive" alt="Certified Cajun certificate">

                        <?php $about_page = get_post(33);
                            $page_url = get_permalink(33);
                            $content = $about_page->post_content;
                            if (str_word_count($content, 0) > 45) {
                                $words = str_word_count($content, 2);
                                $word_pos = array_keys($words);
                                $excerpt = substr($content, 0, $word_pos[45]);
                                echo '<p>' . $excerpt . '...</p><a href="' . $page_url . '" class="btn orange-btn">Read More</a>';
                            } else {
                                echo '<p>' . $content . '</p><a href="' . $page_url . '" class="btn orange-btn">Read More</a>';
                            }
                        ?>
                        <img class="visible-xs-block" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/certified_cajun.png" class="img-responsive" alt="Certified Cajun certificate">

                    </div>
                </div> <!-- .row -->
            </div> <!-- .container -->
        </article>

        <div class="product-carousel">
            <div class="container">
                <div class="row">

                    <div id="product-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner" role="listbox"> <!-- wrapper for slides -->

                        <?php
                            $args = array(
                                'post_type' => 'product',
                                'posts_per_page' => -1,
                                'paged' => $paged,
                                'orderby' => 'menu_order',
                                'order' => 'ASC'
                                );
                            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                            $monocle_products_loop = new WP_Query( $args );
                            if ( $monocle_products_loop->have_posts() ) : ?>
                        <?php while ( $monocle_products_loop->have_posts() ) : $monocle_products_loop->the_post(); ?>

                            <div class="item">
                                <div <?php post_class( $classes ); ?>>
                        
                                    <div class="summary entry-summary col-sm-6 col-sm-push-6 monocle-product-summary">

                                        <?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
                                            <h3 class="title"><?php the_title(); ?></h3>
                                        <?php woocommerce_template_loop_product_link_close(); ?>

                                        <?php if ( $product->has_weight() ) {
                                            echo '<p class="monocle-weight">' . $product->get_weight() . ' ' . esc_attr( get_option('woocommerce_weight_unit') ) . '</p>';
                                        } ?>

                                        <?php do_action( 'monocle_order_page_add_to_cart' ); ?>

                                        <?php woocommerce_template_loop_add_to_cart(); ?>

                                    </div>

                                    <div class="col-sm-6 col-sm-pull-6 monocle-product-img">
                                        <?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
                                            <?php do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
                                        <?php woocommerce_template_loop_product_link_close(); ?>
                                    </div>

                                </div>
                            </div> <!-- .item -->

                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>

                        </div> <!-- .carousel-inner -->

                        <!-- Controls -->
                        <a class="left carousel-control" href="#product-carousel" role="button" data-slide="prev">
                            <div>
                                <span class="fa fa-angle-left"></span>
                                <span class="sr-only">Previous</span>
                            </div>
                        </a>
                        <a class="right carousel-control" href="#product-carousel" role="button" data-slide="next">
                            <div>
                                <span class="fa fa-angle-right"></span>
                                <span class="sr-only">Next</span>
                            </div>
                        </a>

                    </div> <!-- #product-carousel -->

                </div> <!-- .row -->
            </div> <!-- .container -->
        </div> <!-- .product-carousel -->

    </section> <!-- End of the main content section -->

<?php do_action('after_main_content'); ?>
<?php get_footer(); ?>