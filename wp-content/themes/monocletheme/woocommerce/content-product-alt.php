<?php
/**
 *
 * overides the loop for the Sauces page, which is an overide of the
 * native shop page
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();

if ( 0 === ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 === $woocommerce_loop['columns'] ) {
	$classes[] = 'first';
}
if ( 0 === $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
	$classes[] = 'last';
}

$menu_title = get_field('menu_title');
$anchor = str_replace(' ', '-', $menu_title);
$anchor = strtolower($anchor);
?>

	<div class="row">
		<div id="<?php echo $anchor; ?>" class="monocle-hidden-anchor"></div>
		<li <?php post_class( $classes ); ?>>

			<div class="col-sm-5 col-sm-push-7 monocle-thumbnail">

				<?php do_action( 'woocommerce_before_shop_loop_item_title' ); ?>

			</div>

			<div class="col-sm-7 col-sm-pull-5 monocle-description">

				<h3 class="title"><?php echo $menu_title; ?></h3>

				<?php if ( $product->has_weight() ) {
					echo '<p class="monocle-weight">' . $product->get_weight() . ' ' . esc_attr( get_option('woocommerce_weight_unit') ) . '</p>';
				} ?>

				<?php the_excerpt(); ?>

				<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
					<span class="pull-right monocle-read-more">Read More</span>
				<?php woocommerce_template_loop_product_link_close(); ?>

				<?php do_action( 'monocle_order_page_add_to_cart' ); ?>


				<?php woocommerce_template_loop_add_to_cart(); ?>

			</div>

		</li>
	</div> <!-- .row -->

