<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

if ( empty( $product ) || ! $product->exists() ) {
	return;
}

$related = $product->get_related( $posts_per_page );

if ( sizeof( $related ) === 0 ) return;

$args = apply_filters( 'woocommerce_related_products_args', array(
	'post_type'            => 'product',
	'ignore_sticky_posts'  => 1,
	'no_found_rows'        => 1,
	'posts_per_page'       => $posts_per_page,
	'orderby'              => $orderby,
	'post__in'             => $related,
	'post__not_in'         => array( $product->id )
) );

$products = new WP_Query( $args );

$woocommerce_loop['columns'] = $columns;

if ( $products->have_posts() ) : ?>

	<div class="related products">

		<h3><?php _e( 'Related Products', 'woocommerce' ); ?></h3>

			<?php while ( $products->have_posts() ) : $products->the_post(); ?>

				<?php // Manually coppied from content-product.php
				if ( ! defined( 'ABSPATH' ) ) {
					exit; // Exit if accessed directly
				}

				global $product, $woocommerce_loop;

				// Store loop count we're currently on
				if ( empty( $woocommerce_loop['loop'] ) ) {
					$woocommerce_loop['loop'] = 0;
				}

				// Store column count for displaying the grid
				if ( empty( $woocommerce_loop['columns'] ) ) {
					$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
				}

				// Ensure visibility
				if ( ! $product || ! $product->is_visible() ) {
					return;
				}

				// Increase loop count
				$woocommerce_loop['loop']++;

				// Extra post classes
				$classes = array();
				$classes[] = 'col-sm-4';
				if ( 0 === ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 === $woocommerce_loop['columns'] ) {
					$classes[] = 'first';
				}
				if (!( 0 === ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 === $woocommerce_loop['columns'] )) {
					$classes[] = 'hidden-xs';
				}
				if ( 0 === $woocommerce_loop['loop'] % $woocommerce_loop['columns'] ) {
					$classes[] = 'last';
				}
				?>
				<div <?php post_class( $classes ); ?>>

					<?php
					/**
					 * woocommerce_before_shop_loop_item hook.
					 *
					 * @hooked woocommerce_template_loop_product_link_open - 10
					 */
					do_action( 'woocommerce_before_shop_loop_item' );

					if (get_field('menu_title')) {
						$menu_title = get_field('menu_title');
						echo '<h4>' . $menu_title . '</h4>';
					} else {
						echo '<h4>' . get_the_title() . '</h4>';
					}

					/**
					 * woocommerce_before_shop_loop_item_title hook.
					 *
					 * @hooked woocommerce_show_product_loop_sale_flash - 10
					 * @hooked woocommerce_template_loop_product_thumbnail - 10
					 */
					do_action( 'woocommerce_before_shop_loop_item_title' );

					woocommerce_template_loop_product_link_close(); // only outputs </a>
					?>

				</div>

			<?php endwhile; // end of the loop. ?>

	</div>

<?php endif;

wp_reset_postdata();
