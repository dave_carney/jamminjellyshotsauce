<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div class="row">
	<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="col-sm-6 hidden-xs monocle-product-img">
		<?php
			/**
			 * woocommerce_before_single_product_summary hook.
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
		?>
		</div>

		<div class="summary entry-summary col-sm-6 monocle-product-summary">

			<h2 itemprop="name" class="product_title entry-title title"><?php the_title(); ?></h2>

			<div class="visible-xs-block monocle-product-img"><?php do_action( 'monocle_mobile_product_image' ); ?></div>

			<?php global $product; ?>

			<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

				<p class="price"><?php echo $product->get_price_html(); ?>
					
					<?php if ( $product->has_weight() ) {
						echo '<span class="monocle-weight">| ' . $product->get_weight() . ' ' . esc_attr( get_option('woocommerce_weight_unit') ) . '</span>';
					} ?>

				</p>

				<meta itemprop="price" content="<?php echo esc_attr( $product->get_price() ); ?>" />
				<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
				<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

			</div>

			<?php do_action( 'monocle_add_to_cart_button' ); ?>

			<?php the_content(); ?>

		</div><!-- .summary .monocle-product-summary -->


		<meta itemprop="url" content="<?php the_permalink(); ?>" />

	</div><!-- #product-<?php the_ID(); ?> -->
</div> <!-- .row -->

<hr>

<div class="row">
		<?php
			/**
			 * woocommerce_after_single_product_summary hook.
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
		?>
</div> <!-- .row -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
