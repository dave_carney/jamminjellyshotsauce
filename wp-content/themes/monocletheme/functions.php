<?php
	// Cleans up wp_head
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'rel_canonical');
	remove_action('wp_head', 'wp_shortlink_wp_head');
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
	add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');

function disable_emojis_tinymce($plugins) {
	if (is_array($plugins)) {
		return array_diff($plugins, array('wpemoji'));
	} else {
		return array();
	}
}

// Links Stylesheets
function enqueue_styles() {
	wp_register_style('monocle_reset', get_template_directory_uri() . '/css/css_reset.css', array(), null, 'all');
	wp_register_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', array(), null, 'all');
	wp_register_style('fontAwesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css', array(), null, 'all');
	wp_register_style('monocle_style', get_template_directory_uri() . '/css/jelly_style.css', array('monocle_reset', 'bootstrap', 'fontAwesome'), null, 'all');
		wp_enqueue_style('gooogle_fonts', 'https://fonts.googleapis.com/css?family=Lato:400,700|Bitter:400,700', array(), null, 'all');
		wp_enqueue_style('monocle_style');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

// Links Javascript files
function enqueue_scripts() {
	wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', array(), null, true);
	wp_register_script('monocle_scripts', get_template_directory_uri() . '/js/jelly_scripts.js', array('jquery', 'bootstrap'), null, 'true');
		wp_enqueue_script('monocle_scripts');
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');

add_theme_support('post-thumbnails');

//Register menus
function monocle_menus() {
	register_nav_menus(
		array(
			'header-menu' => __('Header Menu'),
			'sidebar-menu' => __('Sidebar Menu'),
			'footer-menu' => __('Footer Menu')
			)
		);
}
add_action('init', 'monocle_menus');

// Register widget areas
function monocle_widget_areas() {
	register_sidebar( array(
		'name'          => 'monocle_widget_area1',
		'id'            => 'widget_area1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
		) 
	);
}
add_action( 'widgets_init', 'monocle_widget_areas' );

// Add image sizes
	add_image_size( 'bootstrap_fullwidth', 1170, 450, true );
	add_image_size( 'homepage_slider', 9999, 600 );

// ACF Options Page
	if (function_exists('acf_add_options_page')) {
		acf_add_options_page( array(
			'page_title' => 'Jammin Jellys Options',
			'menu_title' => 'Options'
			));
	}

// Clean up Admin Sidebar
	$current_user = wp_get_current_user();
	if ( !($current_user->user_login == 'dave' )) {
		add_filter('acf/settings/show_admin', '__return_false');
	}

// Outputs Social items	
function social_output() {
	$facebook = get_field_object('facebook', 'option');
	$twitter = get_field_object('twitter', 'option');
	$instagram = get_field_object('instagram', 'option');

	echo '<li><a href="' . $facebook['value'] . '" title="' . $facebook['label'] . '" target="_blank" class="monocle-social-icon ' . $facebook['name'] . '"><i class="fa fa-' . $facebook['name'] . '"></i><span class="sr-only">' . $facebook['label'] . '</span></a></li>
		<li><a href="' . $twitter['value'] . '" title="' . $twitter['label'] . '" target="_blank" class="monocle-social-icon ' . $twitter['name'] . '"><i class="fa fa-' . $twitter['name'] . '"></i><span class="sr-only">' . $twitter['label'] . '</span></a></li>
		<li><a href="' . $instagram['value'] . '" title="' . $instagram['label'] . '" target="_blank" class="monocle-social-icon ' . $instagram['name'] . '"><i class="fa fa-' . $instagram['name'] . '"></i><span class="sr-only">' . $instagram['label'] . '</span></a></li>';
}

// Monocle Credits
	function monocle_credits() {
		echo '<small class="monocle-credits"><a href="http://www.fallingmonocle.com" target="_blank" title="Falling Monocle Design">Site by Falling Monocle</a></small>';
	}

// Clean up Admin Sidebar
	$current_user = wp_get_current_user();
	if ( !($current_user->user_login == 'dave' )) {

	// Remove admin menu items
		function remove_admin_menu_items() {
			$remove_menu_items = array(__('Comments'), __('Tools'), __('Settings'));
			global $menu;
			end ($menu);
				while (prev($menu)){
					$item = explode(' ',$menu[key($menu)][0]);
					if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
						unset($menu[key($menu)]);}
					}
				}

		add_filter('acf/settings/show_admin', '__return_false');
		add_action('admin_menu', 'remove_admin_menu_items');
	}

// WooCommerce Functions
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

	// Limits related products to 3
	function monocle_related_products_limit( $args ) {
		$args['posts_per_page'] = 3; // 4 related products
		return $args;
	}
	add_filter( 'woocommerce_output_related_products_args', 'monocle_related_products_limit' );

	// Just the add to cart button without the stupid number input
	add_action( 'monocle_add_to_cart_button', 'woocommerce_template_single_add_to_cart' );

	// Outputs the code for the product featured image
	add_action( 'monocle_mobile_product_image', 'woocommerce_show_product_images' );

	// Price output for order page
	add_action( 'monocle_order_page_add_to_cart', 'woocommerce_template_loop_price' );

	// Add to cart button for Order page
	function woocommerce_template_loop_add_to_cart( $args = array() ) {
		global $product;

		if ( $product ) {
			$defaults = array(
				'quantity' => 1,
				'class'    => implode( ' ', array_filter( array(
						'button',
						'btn',
						'orange-btn',
						'product_type_' . $product->product_type,
						$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
						$product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : ''
				) ) )
			);

			$args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( $args, $defaults ), $product );

			wc_get_template( 'loop/add-to-cart.php', $args );
		}
	}
