<?php
/* This file outputs the Header */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<script>document.documentElement.className = document.documentElement.className.replace("no-js","js");</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.png" />

    <title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <meta property="og:title" content="Jammin Jelly's Hot Sauce!">
    <meta property="og:url" content="https://www.jamminjellyshotsauce.com">
    <meta property="og:image" content="https://www.jamminjellyshotsauce.com/wp-content/uploads/2016/06/Hot-sauce-banner.jpg">
    <meta property="og:image:secure_url" content="https://www.jamminjellyshotsauce.com/wp-content/uploads/2016/06/Hot-sauce-banner.jpg">

<?php do_action('in_head'); ?>
    <?php wp_head(); ?>
</head>
<body>
<?php do_action('before_header'); ?>
<header>
	<div id="sticky-nav">
		<div class="container">
			<div class="row">

					<div class="col-sm-6 hidden-xs">
						<a class="sticky-title" href="http://localhost/monocle/jamminjellyshotsauce/">Jammin' Jelly's Hot Sauce</a>
					</div>

					<div class="col-sm-6 col-xs-12 text-right" id="header-nav">
							<?php do_action('before_header_nav'); ?>
								
								<nav class="header-nav">
									<?php if(has_nav_menu('header-menu')) { wp_nav_menu(array('theme_location' => 'header-menu')); } ?>
								</nav>
			
							<?php do_action('after_header_nav'); ?>
					</div>

			</div> <!-- .row -->
		</div> <!-- .container -->
	</div> <!-- #sticky-nav -->

	<div class="container">
		<div class="row">

			<div class="col-xs-9 header-left">
					<?php $logo = get_field('logo', 'option');
						if (!empty($logo)) : ?>
							<div class="logo">
								<a href="http://localhost/monocle/jamminjellyshotsauce/" title="Home">
									<img class="img-responsive" src="<?php echo $logo['url']; ?>" alt="Jammin' Jelly's logo">
									<h1 class="logo-background text-hide">Jammin' Jelly's Hot Sauce</h1>
								</a>
							</div> 

						<?php else: ?>
								<a href="http://localhost/monocle/jamminjellyshotsauce/" title="Home"><h1>Jammin' Jelly's Hot Sauce</h1></a>
						<?php endif; ?>							
			</div>

			<div class="col-xs-3 social">
				<ul>
					<?php social_output(); ?>
				</ul>
			</div>

		</div> <!-- .row -->
	</div> <!-- .container -->

	<div id="static-nav">
		<div class="container">
			<div class="row">

					<div class="col-xs-12 text-right" id="header-nav">
							<?php do_action('before_header_nav'); ?>
								
								<nav class="header-nav">
									<?php if(has_nav_menu('header-menu')) { wp_nav_menu(array('theme_location' => 'header-menu')); } ?>
								</nav>
			
							<?php do_action('after_header_nav'); ?>
					</div>

			</div> <!-- .row -->
		</div> <!-- .container -->
	</div> <!-- #static-nav -->

</header>
